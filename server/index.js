const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const app = express();

const db = require('./db');
const jwt = require('./_helpers/jwt');
const errorHandler = require('./_helpers/error-handler');

// delete data in data Base
// db.sequelize.sync({ force: true}).then(() => {
//     console.log('Drop and resync with { force: true }');
//     userDeafultLoginBackend();
// });

// settigns.
app.set('port', process.env.PORT || '3000');

// middleWares.
app.use(morgan('dev')); // morgan dev
app.use(express.json()); // converter to json
app.use(cors({origin: 'http://localhost:3200', credentials: true})); // connet server angular

// use JWT auth to secure the api
app.use(jwt());

// routes.
app.use(require('./routes/user.routes'));

// global error handler
app.use(errorHandler);

// starting the server.
app.listen(app.get('port'), () => {
    console.log('server on port', app.get('port'));
});

// add tables of Db
function userDeafultLoginBackend () {
    let users = [
        {
            firstName: 'admin',
            lastName: 'admin',
            username: 'admin',
            email: 'admin@android.com',
            password: '1234',
            status: true
        },
        {
            firstName: 'user',
            lastName: 'user',
            username: 'user',
            email: 'user@android.com',
            password: '1234',
            status: true
        },
        {
            firstName: 'staff',
            lastName: 'staff',
            username: 'staff',
            email: 'staff@android.com',
            password: '1234',
            status: 'true'
        }
    ]

    const user = db.user;
    users.forEach(element => {
        user.create(element);
    });
}